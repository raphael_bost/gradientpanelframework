//
//  GPGradientPanel.m
//  GradientPanel
//
//  Created by Raphael Bost on 31/08/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPGradientPanel.h"

#import "GPController.h"

static GPGradientPanel *_sharedGradientPanel = nil;

@implementation GPGradientPanel
+ (GPGradientPanel *)sharedGradientPanel 
{
    if (!_sharedGradientPanel) {
        _sharedGradientPanel = [[GPGradientPanel allocWithZone:[self zone]] init];
    }
    return _sharedGradientPanel;
}

- (id)init 
{
    self = [self initWithWindowNibName:@"GradientPanel"];
    if (self) {
//        [self setWindowFrameAutosaveName:@"GPGradientPanel"];
	}
    return self;
}

- (void)windowDidLoad
{
	[[self window] setTitle:NSLocalizedString(@"Gradient",nil)];
}

- (id)delegate
{
	return _delegate;
}
- (void)setDelegate:(id)del
{
	_delegate = del;
}

- (NSGradient *)gradient
{
	return [[_gradientController gradient] convertGradient];
}
- (void)setGradient:(NSGradient *)grd
{
	[_gradientController setGradient:[CTGradient gradientWithGradient:grd] updateAll:YES];
}

- (CGFloat)angle
{
	return [_gradientController angle];
}
- (void)setAngle:(CGFloat)newAngle
{
	[_gradientController setAngle:newAngle];
}

- (GPGradientType)gradientType
{
	return [_gradientController type];
}

- (void)setGradientType:(GPGradientType)gt
{
	[_gradientController setGradientType:gt];
}
@end

