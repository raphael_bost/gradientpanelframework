//
//  CheckerView.m
//  GradientPanel
//
//  Created by Raphael Bost on 18/10/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "CheckerView.h"


@implementation CheckerView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		_rectList = NULL;
		
		[self setFirstColor:[NSColor whiteColor]];
		[self setSecondColor:[NSColor lightGrayColor]];
		
		[self recalculateSquares];
    }
    return self;
}

- (void)dealloc
{
	[_firstColor release];
	[_secondColor release];
	free(_rectList);
	
	[super dealloc];
}

- (void)drawRect:(NSRect)rect {	
	//remplit le fond
	NSColor* colorA = _firstColor;
	[colorA set];
	NSRectFill(NSIntersectionRect([self checkedRect], rect));
	
	//prepare la deuxième couleur
	NSColor* colorB = _secondColor;
	[colorB set];
		
	//trace les rectangles
	
	[NSGraphicsContext saveGraphicsState];
	[NSBezierPath clipRect:[self checkedRect]];
	NSRectFillList(_rectList, _rectNumbers);	

	[NSGraphicsContext restoreGraphicsState];
}


- (void)setFrame:(NSRect)rect
{
	if(!NSEqualSizes(rect.size, [self frame].size)){
		[self recalculateSquares];
	}
	[super setFrame:rect];
	[self setNeedsDisplay:YES];
}

/*- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize
{
	
}*/

- (void)recalculateSquares
{
	free(_rectList);
	
	NSRect rect = [self checkedRect];
	
	//Calcule les rectangles à tracer en fonction du rect courant
	const CGFloat rectSize = 5; //taille d'un carreau du damier
	const NSInteger minXIndex = (NSInteger) (rect.origin.x / rectSize);
	const NSInteger maxXIndex = (NSInteger) ceil((rect.origin.x + rect.size.width) / rectSize);
	const NSInteger minYIndex = (NSInteger) (rect.origin.y / rectSize);
	const NSInteger maxYIndex = (NSInteger) ceil((rect.origin.y + rect.size.height) / rectSize);
	const NSInteger nbRects = (maxXIndex-minXIndex+1)*(maxYIndex-minYIndex+1); //à peaufiner (surévalué ici)
	
	_rectList = nbRects ? (NSRect*) malloc(nbRects*sizeof(NSRect)) : NULL;
	NSInteger exactNbRects = 0;
	NSInteger i = 0;
	NSInteger j = 0;
	for(j=minYIndex ; j<=maxYIndex ; ++j)
	{
		for(i=minXIndex + (j%2) ; //le coup du %2, c'est pour décaler d'une ligne sur l'autre
			i<=maxXIndex ; i += 2)
		{
			_rectList[exactNbRects++] = NSMakeRect(i*rectSize, j*rectSize, rectSize, rectSize);
		}
	}	
	
	_rectNumbers = exactNbRects;
}


- (NSColor *)firstColor
{
	return _firstColor;
}
- (void)setFirstColor:(NSColor *)color
{
	[color retain];
	[_firstColor release];
	_firstColor = color;
	[self setNeedsDisplay:YES];
}

- (NSColor *)secondColor
{
	return _secondColor;
}
- (void)setSecondColor:(NSColor *)color
{
	[color retain];
	[_secondColor release];
	_secondColor = color;
	[self setNeedsDisplay:YES];
}

- (NSRect)checkedRect
{
	return [self bounds];
}
@end
