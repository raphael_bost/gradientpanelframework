//
//  GPController.m
//  GradientPanel
//
//  Created by Raphael Bost on 27/08/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPController.h"

@implementation GPController
- (id)init
{
	self = [super init];
	
	_gradient = [[CTGradient gradientWithBeginningColor:[NSColor whiteColor] endingColor:[NSColor blackColor]] retain];
//	[_gradient addColor:[NSColor greenColor] at:0.5]; 
	return self;
}

- (void)dealloc
{
	[_gradient release];
	
	[super dealloc];
}
- (CTGradient *)gradient
{
	return _gradient;
}

- (void)setGradient:(CTGradient *)grd
{
	[grd retain];
	[_gradient release];
	_gradient = grd;
	
	[_grdView setNeedsDisplay:YES];
//	[_grdLine updateGradientLine];
	
	[[_gpController delegate] changeGradient:self];

}

- (void)setGradient:(CTGradient *)grd updateAll:(BOOL)flag
{
	[self setGradient:grd];
	
	if(flag){
		[_grdLine updateGradientLine];
		[_angleSlider setFloatValue:[self angle]];

	}
}

- (CGFloat)angle
{
	return _angle;
}

- (void)setAngle:(CGFloat)newAngle
{
	_angle = newAngle;
	[_grdView setNeedsDisplay:YES];
		
	[[_gpController delegate] changeGradient:self];
}

- (GPGradientType)type
{
	return _type;
}

- (void)setGradientType:(GPGradientType)gt
{
	_type = gt;
	[_grdView setNeedsDisplay:YES];
	
	[_typeChooser setSelectedSegment:gt];
	
	[[_gpController delegate] changeGradient:self];
}

- (IBAction)takeAngleValueFrom:(id)sender
{
	[self setAngle:-[sender floatValue]];
}

- (IBAction)takeTypeFrom:(id)sender
{
	[self setGradientType:[sender selectedSegment]];
}

- (void)updateViews
{
	[_grdLine setNeedsDisplay:YES];
	[_grdView setNeedsDisplay:YES];
}

- (void)writeGradientToPasteboard:(NSPasteboard *)pb
{
	NSDictionary *dict;
	NSData *gradientData;

	[pb declareTypes:[NSArray arrayWithObject:@"GradientPboardType"] owner:self];
	
	dict = [NSDictionary dictionaryWithObjectsAndKeys:[[self gradient] convertGradient],@"Gradient",
			[NSNumber numberWithInt:[self type]],@"Type",
			[NSNumber numberWithFloat:[self angle]],@"Angle",
			nil];
	
	//dict = [NSDictionary dictionaryWithObject:[[self gradient] convertGradient] forKey:@"Gradient"];
	
	gradientData = [NSKeyedArchiver archivedDataWithRootObject:dict];
	
	[pb setData:gradientData forType:@"GradientPboardType"];		
}

- (BOOL)readGradientFromPasteboard:(NSPasteboard *)pb
{
	NSData *pbData;
	NSString *type;
	NSDictionary *dict;
	
	type = [pb availableTypeFromArray:[NSArray arrayWithObject:@"GradientPboardType"]];
	
	if(type){
		pbData = [pb dataForType:@"GradientPboardType"];
	   	dict = [NSKeyedUnarchiver unarchiveObjectWithData:pbData];
		
		[self setGradient:[CTGradient gradientWithGradient:[dict objectForKey:@"Gradient"]] updateAll:YES];
		[self setGradientType:[[dict objectForKey:@"Type"] intValue]];
		[self setAngle:[[dict objectForKey:@"Angle"] floatValue]];
		
		return YES;
	}
	
	return NO;
}
@end
