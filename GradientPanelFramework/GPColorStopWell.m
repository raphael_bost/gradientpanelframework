//
//  GPColorStopWell.m
//  GradientPanel
//
//  Created by Raphael Bost on 29/08/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPColorStopWell.h"
#import "CTGradient.h"

#define GPArrowHeight 5.0
#define GPRoundRadius 3.0

NSString *GPSelectedColorStopDidChange = @"GPSelectedColorStopDidChange";

@implementation GPColorStopWell

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(colorChanged:) name:NSColorPanelColorDidChangeNotification object:nil];		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateColorStop:) name:GPSelectedColorStopDidChange object:nil];
	}
    return self;
}

- (void)dealloc
{
	[_color release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}

- (void)drawRect:(NSRect)rect {
    // Drawing code here.
/*	[_color drawSwatchInRect:[self bounds]];
	
	NSRect border;
	border = NSInsetRect([self bounds], 1.0, 1.0);
	[[NSColor redColor] set];
	NSBezierPath *path;
	path = [NSBezierPath bezierPathWithRect:border];
	[path setLineWidth:2.0];
	[path stroke];
	
 */
	NSBezierPath *path;
	path = [NSBezierPath bezierPath];
	[path moveToPoint:NSMakePoint(GPRoundRadius+0.5, [self frame].size.height-0.5)];
	[path lineToPoint:NSMakePoint([self frame].size.width-GPRoundRadius - 1.0, [self frame].size.height-0.5)];
	[path appendBezierPathWithArcWithCenter:NSMakePoint([self frame].size.width-GPRoundRadius-1.0, [self frame].size.height-GPRoundRadius-0.5) radius:GPRoundRadius+0.5 startAngle:90.0 endAngle:0.0 clockwise:YES];
	[path lineToPoint:NSMakePoint([self frame].size.width -1.0, GPArrowHeight)];
	[path lineToPoint:NSMakePoint([self frame].size.width/2.0,0.5)];
	[path lineToPoint:NSMakePoint(0.5, GPArrowHeight)];
	[path lineToPoint:NSMakePoint(0.5, [self frame].size.height-GPArrowHeight)];
	[path appendBezierPathWithArcWithCenter:NSMakePoint(GPRoundRadius+0.5, [self frame].size.height-GPRoundRadius) radius:GPRoundRadius startAngle:180.0 endAngle:90.0 clockwise:YES];
	[path closePath];
	
	if(_isActive){
		[[CTGradient unifiedPressedGradient] fillBezierPath:path angle:0.0];
//		[[NSColor redColor] set];
//		[path fill];
	}else{
		[[CTGradient unifiedNormalGradient] fillBezierPath:path angle:0.0];
	}
	
	[[NSColor colorWithDeviceWhite:0.5 alpha: 0.7] set];
	[path stroke];
	NSBezierPath *line = [NSBezierPath bezierPath];
	[line moveToPoint:NSMakePoint(2.5, GPArrowHeight+0.5)];
	[line lineToPoint:NSMakePoint([self frame].size.width -2.5, GPArrowHeight+0.5)];
	[line stroke];
	
	//[_color set];
	//[NSBezierPath fillRect:NSMakeRect(2.0, GPArrowHeight+1.0,[self frame].size.width - 4.0, [self frame].size.height-GPArrowHeight - GPRoundRadius)];
	[_color drawSwatchInRect:NSMakeRect(2.0, GPArrowHeight+1.0,[self frame].size.width - 4.0, [self frame].size.height-GPArrowHeight - GPRoundRadius)];
}

- (NSColor *)color;
{
	return _color;
}

- (void)setColor:(NSColor *)color
{
	if([color isEqual:_color])
		return;
	
	[color retain];
	[_color release];
	_color = color;
	
	[_owner setColor:color forStop:self];
	
	[self setNeedsDisplay:YES];
}

- (GPGradientSlider *)owner
{
	return _owner;
}
- (void)setOwner:(GPGradientSlider *)slider
{
	_owner = slider;
}

- (void)mouseDown:(NSEvent *)theEvent
{
	_isActive = !_isActive;
	
	if(_isActive){
		[[self window] makeFirstResponder:self];
		[[NSNotificationCenter defaultCenter] postNotificationName:GPSelectedColorStopDidChange object:self];
	}
}
- (void)mouseDragged:(NSEvent *)theEvent
{
	_hasDragged = YES;
	_isActive = YES;
	NSRect frame;
	float oldX;
	frame = [self frame];
	oldX = frame.origin.x;
	frame.origin.x += [theEvent deltaX];
	
	frame.origin.x = MAX(0.0, frame.origin.x);
	frame.origin.x = MIN(frame.origin.x, [_owner frame].size.width - [self frame].size.width);

	[self setFrame:frame];
	
	[_owner updateLocationForStop:self deltaPos:(frame.origin.x - oldX)];
}

- (void)mouseUp:(NSEvent *)theEvent
{

	if(!_hasDragged && _isActive){
		[[NSColorPanel sharedColorPanel] makeKeyAndOrderFront:self];
		[[NSColorPanel sharedColorPanel] setColor:[[[self color] copy] autorelease]];
		//_isActive = YES;
	}else{
		_isActive = NO;
	}
	_hasDragged = NO;
	[self setNeedsDisplay:YES];
}

- (void)keyDown:(NSEvent *)theEvent
{
	if([theEvent keyCode] == 51 && _isActive){
		[self removeStop:self];
	}
}

- (void)colorChanged:(NSNotification *)note 
{
	if(_isActive){
		[self setColor:[[NSColorPanel sharedColorPanel] color]];
	}
}

- (void)updateColorStop:(NSNotification *)note
{
	if([note object] == self){
		//_isActive = YES;
	}else if([note object] == nil || [[note object] owner] == _owner){
		_isActive = NO;
		[self setNeedsDisplay:YES];
	}
}

- (void)removeStop:(id)sender
{
	[_owner removeStop:self];
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{                  
	NSMenu *menu;
	menu = [[NSMenu alloc] initWithTitle:@"Stop Menu"];
	
	NSMenuItem *item;
	
	item = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Remove Stop",nil) action:@selector(removeStop:) keyEquivalent:@""];
	[item setTarget:self];
	[menu insertItem:item atIndex:0];
	
	[item release];
	
	return [menu autorelease];
}
@end
