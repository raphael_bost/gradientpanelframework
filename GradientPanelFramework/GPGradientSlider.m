//
//  GPGradientSlider.m
//  GradientPanel
//
//  Created by Raphael Bost on 27/08/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPGradientSlider.h"

#import "GPController.h"
#import "GPColorStopWell.h"

#define LIMIT( value, min, max )		(((value) < (min))? (min) : (((value) > (max))? (max) : (value)))


@implementation GPGradientSlider

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)awakeFromNib
{
	[self updateGradientLine];
}
- (void)drawRect:(NSRect)rect {
    // Drawing code here.
	
	NSBezierPath *boundary;
	NSRect boundRect, grdRect;
	
	boundRect = [self bounds];
	boundRect.size.height -= 6.0;

	boundary = [NSBezierPath bezierPathWithRect:NSInsetRect(boundRect,3.5,0.5)];
	[boundary setLineWidth:1.0];

	[[NSColor whiteColor] set];
	[boundary fill];
	[[NSColor grayColor] set];
	[boundary stroke];
	
	grdRect = NSMakeRect(6.0, 3.0, [self bounds].size.width-12.0, [self bounds].size.height - 12);
	
//	[[CTGradient aquaNormalGradient] fillRect:NSInsetRect([self bounds],0.5, 0.5) angle:90.0];

	[super drawRect:rect];
	[[_controller gradient] fillRect:grdRect angle:0.0];
	
	[NSBezierPath strokeRect:NSInsetRect(grdRect,0.5,0.5)];
}

- (void)updateGradientLine
{
	CTGradient *grd;
	grd = nil;
	grd = [_controller gradient];

	[[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[_colorStops release];
	_colorStops = [[NSMutableArray alloc] init];
		
	NSUInteger i;
	CGFloat location;
	GPColorStopWell *colorWell;
	CTGradientElement *colorStop;
	
	for (i = 0; [grd elementAtIndex:i]; i++) {
		colorStop = [grd elementAtIndex:i];
		location = colorStop->position;

		colorWell = [[GPColorStopWell alloc] initWithFrame:NSMakeRect(location*([self frame].size.width - 12 ), [self bounds].size.height - 15, 12, 15)];
		[colorWell setOwner:self];
		[colorWell setTag:i];
		[_colorStops addObject:colorWell];
		[colorWell release];
		[colorWell setColor:[grd colorStopAtIndex:i]];
		
		[self addSubview:colorWell];
	}
	[self setNeedsDisplay:YES];
}

- (void)updateTags
{
	
}

- (void)updateLocationForStop:(GPColorStopWell *)well deltaPos:(CGFloat)deltaPos
{
	CGFloat location;
	CTGradient *grd;
//	NSRect frame;
	grd = [_controller gradient];
	int index;
	NSInteger i;
	i = [well tag];

	
	location = ([well frame].origin.x+deltaPos) / ([self frame].size.width - 12.0);
	index = [grd elementIndexForPosition:location];
	
	NSColor *color; 
	color = [[[_controller gradient] colorStopAtIndex:[well tag]] copy];
	
	grd = [grd removeColorStopAtIndex:[well tag]];
	grd = [grd addColorStop:color atPosition:location];

	[color release];
	
	if(index < [well tag] && deltaPos < 0){
		[well setTag:i-1];
		[[_colorStops objectAtIndex:i-1] setTag:i];
		[_colorStops exchangeObjectAtIndex:i withObjectAtIndex:i-1];
		
		i = i-1;
	}else if(deltaPos > 0 && index > ([well tag]+1)){
		[well setTag:i+1];
		[[_colorStops objectAtIndex:i+1] setTag:i];
		[_colorStops exchangeObjectAtIndex:i withObjectAtIndex:i+1];
		
		i = i+1;
	}
	
	[_controller setGradient:grd];	
	[_controller updateViews];
}

- (void)setColor:(NSColor *)color forStop:(GPColorStopWell *)well
{
	CGFloat location;
	CTGradient *grd;

	grd = [_controller gradient];
	location = [grd elementAtIndex:[well tag]]->position;

	grd = [grd removeColorStopAtIndex:[well tag]];
	grd = [grd addColorStop:color atPosition:location];
	
	[_controller setGradient:grd];	
	[_controller updateViews];
}

- (void)mouseDown:(NSEvent *)theEvent
{
	CGFloat location;
	location = ([self convertPoint:[theEvent locationInWindow] fromView:nil].x - 6.0) / ([self frame].size.width - 12);
	location = LIMIT(location,0.0, 1.0);
	if([theEvent clickCount] == 2 && NSPointInRect([self convertPoint:[theEvent locationInWindow] fromView:nil], [self checkedRect])){
		[self addColorStopAtLocation:location];
	}else{
		[[NSNotificationCenter defaultCenter] postNotificationName:GPSelectedColorStopDidChange object:nil];
	}
}

- (void)addColorStopAtLocation:(CGFloat)location
{
	[_controller setGradient:[[_controller gradient] addColorStop:[[_controller gradient] colorAtPosition:location] atPosition:location]];
	
	[self updateGradientLine];
	[_controller updateViews];	
}

- (void)removeStop:(GPColorStopWell *)stop
{
	CGFloat location;
	
	location = ([stop frame].origin.x) / ([self frame].size.width - 12.0);
	
	[_controller setGradient:[[_controller gradient] removeColorStopAtPosition:location]];
	
	
	[self updateGradientLine];
}
- (NSRect)checkedRect
{
	return NSMakeRect(6.0, 3.0, [self bounds].size.width-12.0, [self bounds].size.height - 12);
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{                  
	_menuClickLocation = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	NSMenu *menu;
	menu = [[NSMenu alloc] initWithTitle:@"Slider Menu"];
	
	NSMenuItem *item;
	
	item = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Add Stop",nil) action:@selector(addStop:) keyEquivalent:@""];
	[item setTarget:self];
	[menu insertItem:item atIndex:0];

	[menu insertItem:[NSMenuItem separatorItem] atIndex:1];
	
	item = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Copy",nil) action:@selector(copyGradient:) keyEquivalent:@""];
	[item setTarget:self];
	[menu insertItem:item atIndex:2];
	
	[item release];
	
	return [menu autorelease];
}

- (IBAction)addStop:(id)sender
{
	CGFloat location;
	location = (_menuClickLocation.x - 6.0) / ([self frame].size.width - 12);
	location = LIMIT(location,0.0, 1.0);	
	[self addColorStopAtLocation:location];
}

- (IBAction)copyGradient:(id)sender
{
	[_controller writeGradientToPasteboard:[NSPasteboard pasteboardWithName:NSGeneralPboard]];
}
@end
