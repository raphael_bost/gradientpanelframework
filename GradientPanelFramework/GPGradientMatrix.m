//
//  GPGradientMatrix.m
//  GradientPanel
//
//  Created by Raphael Bost on 26/10/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPGradientMatrix.h"

#import "GPGradientCell.h"

@implementation GPGradientMatrix
- (Class)cellClass
{
	return [GPGradientCell class];
}

- (id)initWithFrame:(NSRect)frameRect
{
	self = [super initWithFrame:frameRect];
	
	[self setCellSize:NSMakeSize(32, 32)];
	return self;
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{                        
	[super menuForEvent:theEvent];
	
	NSMenu *menu;
	menu = [[NSMenu alloc] initWithTitle:@"Matrix Menu"];
	
	NSMenuItem *item;
	
	item = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Remove",nil) action:@selector(remove:) keyEquivalent:@""];
	[item setTarget:self];
	[menu insertItem:item atIndex:0];
	
	[item release];
	
	return [menu autorelease];
}
@end
