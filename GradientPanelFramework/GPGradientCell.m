//
//  GPGradientCell.m
//  GradientPanel
//
//  Created by Raphael Bost on 25/05/07.
//  Copyright 2007 Raphael Bost. All rights reserved.
//

#import "GPGradientCell.h"

#import "CTGradient.h"

@implementation GPGradientCell
- (void)dealloc
{
	[_gradient release];
	
	[super dealloc];
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
  	NSBezierPath *path = [NSBezierPath bezierPathWithRect:NSInsetRect(cellFrame,0.5, 0.5)];
  
/*  	[[NSColor whiteColor] set];
	[path fill];
*/	
	
	if([self isActive])
		[[CTGradient unifiedPressedGradient] fillRect:NSInsetRect(cellFrame,0.5, 0.5) angle:90.0];
	else
		[[CTGradient unifiedSelectedGradient] fillRect:NSInsetRect(cellFrame,0.5, 0.5) angle:90.0];

  	[[NSColor lightGrayColor] set];
	[path stroke];
	
/*	if(_type == GPLinearType)
		[_gradient fillRect:NSInsetRect(cellFrame,3,3) angle:_angle];
	else
		[_gradient radialFillRect:NSInsetRect(cellFrame,3,3)];
*/
	
	if(_type == GPLinearType)
		[_gradient drawInRect:NSInsetRect(cellFrame,3,3) angle:_angle];
	else
		[_gradient drawInRect:NSInsetRect(cellFrame,3,3) relativeCenterPosition:NSZeroPoint];
	
}

- (NSGradient *)gradient
{
	return _gradient;
}

- (void)setGradient:(NSGradient *)newGradient
{   
	[newGradient retain];
	[_gradient release];
	_gradient = newGradient;
}

- (CGFloat)angle
{
	return _angle;
}

- (void)setAngle:(CGFloat)newAngle
{
	_angle = newAngle;
}

- (GPGradientType)gdType
{
	return _type;
}

- (void)setGdType:(GPGradientType)gt
{
	_type = gt;
}

- (id)objectValue
{
	if(_gradient){
		return [NSDictionary dictionaryWithObjectsAndKeys:[self gradient] ,@"Gradient",
														  [NSNumber numberWithInt:[self gdType]],@"Type",
														  [NSNumber numberWithFloat:[self angle]],@"Angle",
														  nil];
	}
	return nil;
}

- (void)setObjectValue:(id)object
{
	[self setGradient:[object objectForKey:@"Gradient"]];
	[self setGdType:[[object objectForKey:@"Type"] intValue]];
	[self setAngle:[[object objectForKey:@"Angle"] floatValue]];
}
- (BOOL)isActive
{
	return _isActive;
}

- (void)activate:(BOOL)flag
{
	_isActive = flag;
	[self setHighlighted:flag];
}

@end
