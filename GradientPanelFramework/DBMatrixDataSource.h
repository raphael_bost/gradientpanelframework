/*
 *  DBMatrixDataSource.h
 *  DBColorSwatchApp
 *
 *  Created by Raphael Bost on 08/07/08.
 *  Copyright 2008 Raphael Bost. All rights reserved.
 *
 */
 
@protocol DBMatrixDataSource 
- (Class)cellClass;

- (NSUInteger)numberOfObjects;
- (id)objectAtIndex:(NSUInteger)index;
- (void)addObject:(id)object;
- (void)removeObject:(id)object;

- (id)readObjectFromPasteboard:(NSPasteboard *)pb;                                   
- (void)writeObject:(id)object toPasteboard:(NSPasteboard *)pb;

- (NSArray *)draggedTypes;
- (void)dragObject:(id)object withEvent:(NSEvent *)theEvent pasteBoard:(NSPasteboard *)pboard;
@end
