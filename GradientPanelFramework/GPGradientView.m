//
//  GPGradientView.m
//  GradientPanel
//
//  Created by Raphael Bost on 27/08/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPGradientView.h"

#import "GPController.h"

@implementation GPGradientView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		[self registerForDraggedTypes:[NSArray arrayWithObjects:@"GradientPboardType",  nil]];
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    // Drawing code here.
	NSBezierPath *boundary;
	
	boundary = [NSBezierPath bezierPathWithRoundedRect:NSInsetRect([self bounds], 2.0, 2.0) xRadius:8.0 yRadius:8.0];
	[boundary setLineWidth:2.0];
	[[NSColor grayColor] set];
	[boundary stroke];
	
	[NSGraphicsContext saveGraphicsState];
	
	[boundary addClip];
	
	[super drawRect:rect];
	
	if([_controller type] == GPLinearType)
		[[self gradient] fillRect:[self bounds] angle:[self angle]];
	else
		[[self gradient] radialFillRect:[self bounds]];
	
	[NSGraphicsContext restoreGraphicsState];
}	

- (CTGradient *)gradient
{
	return [_controller gradient];
}

- (void)setGradient:(CTGradient *)grd
{
	[_controller setGradient:grd];
}

- (CGFloat)angle
{
	return [_controller angle];
}

- (void)setAngle:(CGFloat)newAngle
{
	[_controller setAngle:newAngle];
}

- (void)mouseDown:(NSEvent *)theEvent
{
	[_controller writeGradientToPasteboard:[NSPasteboard pasteboardWithName:NSDragPboard]];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	NSImage *image;
	NSPoint point;
	NSRect rect;
		
	point = [self convertPoint:[theEvent locationInWindow] fromView:nil];	
	
	rect.origin = NSZeroPoint;
	rect.size = NSMakeSize(31, 31);
	
	image = [[NSImage alloc] initWithSize:NSMakeSize(31, 31)];
	[image setFlipped:NO];
	
	[image lockFocus];
	NSAffineTransform *at;
	at = [NSAffineTransform transform];
	//	[at translateXBy:-cellFrame.origin.x yBy:-cellFrame.origin.y];
	
	[NSGraphicsContext saveGraphicsState];
	[at concat];
	
	
	if([_controller type] == GPLinearType)
		   [[self gradient] fillRect:rect angle:[self angle]];
	else
		   [[self gradient] radialFillRect:rect];
		   
	[NSGraphicsContext restoreGraphicsState];
	
	[image unlockFocus];
	
	point.x -= [image size].width/2;
	point.y -= [image size].height/2;
	
	
	[self dragImage:image at:point offset:NSMakeSize(0,0) event:theEvent pasteboard:[NSPasteboard pasteboardWithName:NSDragPboard] source:self slideBack:NO];
	
	[image release];	
	
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
 	return NSDragOperationLink;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{                     	
	return [_controller readGradientFromPasteboard:[sender draggingPasteboard]];
}

- (unsigned int)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	return NSDragOperationNone;
}

@end