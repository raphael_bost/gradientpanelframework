//
//  GPGradientShelfController.m
//  GradientPanel
//
//  Created by Raphael Bost on 26/10/08.
//  Copyright 2008 Raphael Bost. All rights reserved.
//

#import "GPGradientShelfController.h"


@implementation GPGradientShelfController
+ (NSString *)gradientShelfDirectory 
{
	NSString *gradientDir;
	NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, /* expandTilde */ YES);
	
	gradientDir = [searchPath objectAtIndex:0];
	gradientDir = [gradientDir stringByAppendingString:@"/Gradients"];
	
	if(![[NSFileManager defaultManager] fileExistsAtPath:gradientDir isDirectory:NULL]){
        [[NSFileManager defaultManager] createDirectoryAtPath:gradientDir withIntermediateDirectories:YES attributes:nil error:NULL];
	}
	
	return gradientDir;
}                

- (id)init
{
	self = [super init];
	
	_gradients = [[NSMutableArray alloc] init];
	
	return self;
}

- (void)dealloc
{
	[_gradients release];
	
	[super dealloc];
}

- (void)awakeFromNib
{	
	[self readGradientShelf];
	[_matrix reloadData];
}                                 

- (void)readGradientShelf
{
	NSString *gradientDir;
	NSString *gradientFile;

	gradientDir = [GPGradientShelfController gradientShelfDirectory];
	gradientFile = [NSString stringWithFormat:@"%@/%@.grdlib",gradientDir,@"Gradients",nil];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:gradientFile]) {
		[_gradients release];
		NSData *data;
		data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:gradientFile]];
		_gradients = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];		
	}
			
//	NSLog(@"data %@",data);
//	NSLog(@"gradients %@",_gradients);
	//	_gradients = [[NSMutableArray alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.grdlib",gradientDir,@"Gradients",nil]]];
}

- (void)writeGradientShelf
{
	NSString *gradientDir;
	NSString *fileName;
	
	gradientDir = [GPGradientShelfController gradientShelfDirectory];
	fileName = [NSString stringWithFormat:@"%@/%@.grdlib",gradientDir,@"Gradients",nil];

	if(![[NSKeyedArchiver archivedDataWithRootObject:_gradients] writeToFile:fileName
					 atomically:NO]){
		NSBeep();
		NSLog(@"error when writing gradient shelf");
	}
}

- (NSUInteger) numberOfObjects
{
	return [_gradients count];
}

- (id)objectAtIndex:(int)index
{
	return [_gradients objectAtIndex:index];
}

- (void)addObject:(id)object 
{
	
	[_gradients addObject:[[object copy] autorelease]];
	
	[self writeGradientShelf];
	
	[_matrix reloadData];
}

- (void)removeObject:(id)object
{
	[_gradients removeObjectAtIndex:[_gradients indexOfObject:object]];
	[self writeGradientShelf];
}

- (id)readObjectFromPasteboard:(NSPasteboard *)pb
{
	NSData *pbData;
	NSString *type;
	id unarchivedData;
	
	type = [pb availableTypeFromArray:[NSArray arrayWithObject:@"GradientPboardType"]];
	
	if(type){
		pbData = [pb dataForType:@"GradientPboardType"];
	   	unarchivedData = [NSKeyedUnarchiver unarchiveObjectWithData:pbData];
		
		return unarchivedData;
	}
	return nil;	
}

- (void)writeObject:(id)object toPasteboard:(NSPasteboard *)pb
{
	if(!object){
		return;
	}
	[pb declareTypes:[NSArray arrayWithObject:@"GradientPboardType"] owner:self];
	
	NSData *gradientData;
	gradientData = [NSKeyedArchiver archivedDataWithRootObject:object];
	
	[pb setData:gradientData forType:@"GradientPboardType"];
}

- (NSArray *)draggedTypes
{
	return [NSArray arrayWithObjects:@"GradientPboardType",  nil];
}

- (void)dragObject:(id)object withEvent:(NSEvent *)theEvent pasteBoard:(NSPasteboard *)pboard
{
	NSImage *image;
	NSPoint point;
	NSRect rect;

	NSGradient *grd = nil;
	int type = 0;
	float angle = 0.0;
	
	point = [_matrix convertPoint:[theEvent locationInWindow] fromView:nil];
	
	grd = [object objectForKey:@"Gradient"];
	type = [[object objectForKey:@"Type"] intValue];
	angle = [[object objectForKey:@"Angle"] floatValue];

	
	rect.origin = NSZeroPoint;
	rect.size = NSMakeSize(31, 31);
	
	image = [[NSImage alloc] initWithSize:[_matrix cellSize]];
	[image setFlipped:NO];
	
	[image lockFocus];
	NSAffineTransform *at;
	at = [NSAffineTransform transform];
//	[at translateXBy:-cellFrame.origin.x yBy:-cellFrame.origin.y];
	
	[NSGraphicsContext saveGraphicsState];
	[at concat];
	
	
	if(type == 0)
		[grd drawInRect:rect angle:angle];
	else
		[grd drawInRect:rect relativeCenterPosition:NSZeroPoint];
	
	[NSGraphicsContext restoreGraphicsState];
	
	[image unlockFocus];
	
	point.x -= [image size].width/2;
	point.y += [image size].height/2;
	
	
	[_matrix dragImage:image at:point offset:NSMakeSize(0,0) event:theEvent pasteboard:[NSPasteboard pasteboardWithName:NSDragPboard] source:_matrix slideBack:NO];
	
	[image release];
}
@end
