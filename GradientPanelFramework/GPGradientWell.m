//
//  GPGradientWell.m
//  GradientPanel
//
//  Created by Raphael Bost on 25/05/07.
//  Copyright 2007 Raphael Bost. All rights reserved.
//

#import "GPGradientWell.h"

#import "GPGradientCell.h"

#import "GPGradientPanel.h"

@implementation GPGradientWell
+ (Class)cellClass
{
	return [GPGradientCell class];
}   

+ (void)initialize
{
	[self exposeBinding:@"gradient"];
	[self exposeBinding:@"angle"];
	[self exposeBinding:@"gradientType"];
	[self exposeBinding:@"value"];
}

- (id)initWithFrame:(NSRect)frame
{
	self = [super initWithFrame:frame];
		            
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(gradientPanelWillClose:) 
												 name:NSWindowWillCloseNotification 
											   object:[[GPGradientPanel sharedGradientPanel] window]];
	[self registerForDraggedTypes:[NSArray arrayWithObjects:@"GradientPboardType",  nil]];

	return self;
}
     
- (void)mouseDown:(NSEvent *)theEvent
{
//	NSLog(@"down");
}

- (void)mouseUp:(NSEvent *)theEvent
{
	[self activate:![self isActive]];
	[self setNeedsDisplay:YES]; 
	
	if([self isActive]){
		[[[GPGradientPanel sharedGradientPanel] window] makeKeyAndOrderFront:self];
	}else{
	}
}

- (void)gradientPanelWillClose:(NSNotification *)note
{
	[self activate:NO];
	[self setNeedsDisplay:YES];
	
	[self setGradient:[[GPGradientPanel sharedGradientPanel] gradient]];
}

- (void)changeGradient:(id)object
{ 
	[self setGradient:[[GPGradientPanel sharedGradientPanel] gradient]];
	
	[self willChangeValueForKey:@"gradientAngle"];
	[[self cell] setAngle:[[GPGradientPanel sharedGradientPanel] angle]];
	[self didChangeValueForKey:@"gradientAngle"];

	[self willChangeValueForKey:@"gradientType"];
	[[self cell] setGdType:[[GPGradientPanel sharedGradientPanel] gradientType]];
	[self didChangeValueForKey:@"gradientType"];
	
	[NSApp sendAction:[self action] to:[self target] from:self];
}

- (NSGradient *)gradient
{
	return [[self cell] gradient];
}

- (void)setGradient:(NSGradient *)newGradient
{   
	[self willChangeValueForKey:@"gradient"];
	[[self cell] setGradient:newGradient];
	[self setNeedsDisplay:YES];
	[self didChangeValueForKey:@"gradient"];
} 

- (CGFloat)gradientAngle
{
	return [[self cell] angle];
}

- (void)setGradientAngle:(CGFloat)angle
{   
	[self willChangeValueForKey:@"gradientAngle"];
	[[self cell] setAngle:angle];
	[self setNeedsDisplay:YES];
	[self didChangeValueForKey:@"gradientAngle"];
} 

- (GPGradientType)gradientType
{
	return [[self cell] gdType];
}

- (void)setGradientType:(GPGradientType)type
{   
	[self willChangeValueForKey:@"gradientType"];
	[[self cell] setGdType:type];
	[self setNeedsDisplay:YES];
	[self didChangeValueForKey:@"gradientType"];
} 


- (BOOL)isFlipped
{
	return NO;
}

- (BOOL)acceptsFirstMouse:(NSEvent *)mouseDownEvent
{
	return YES;
}

- (id)objectValue
{
	return [self gradient];
}

- (void)setObjectValue:(id)newObjectValue
{
	[self setGradient:newObjectValue];
}

- (BOOL)isActive
{
	return [_cell isActive];
}

- (void)activate:(BOOL)flag
{
	[_cell activate:flag];

	if(![self isActive] && [[GPGradientPanel sharedGradientPanel] delegate] == self){
		[[GPGradientPanel sharedGradientPanel] setDelegate:nil];
	}else if([self isActive]){
		[[[GPGradientPanel sharedGradientPanel] delegate] deactivate];
		
		[[GPGradientPanel sharedGradientPanel] setGradient:[self gradient]];
		[[GPGradientPanel sharedGradientPanel] setAngle:[[self cell] angle]];
		[[GPGradientPanel sharedGradientPanel] setGradientType:[[self cell] gdType]];
		[[GPGradientPanel sharedGradientPanel] setDelegate:self];		
	}
		
	[self setNeedsDisplay:YES];
}

- (void)deactivate
{
	[self activate:NO];
}


- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
 	return NSDragOperationLink;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{                     	
	NSData *pbData;
	NSString *type;
	NSDictionary *dict;
	
	type = [[sender draggingPasteboard] availableTypeFromArray:[NSArray arrayWithObject:@"GradientPboardType"]];
	
	if(type){
		pbData = [[sender draggingPasteboard] dataForType:@"GradientPboardType"];
	   	dict = [NSKeyedUnarchiver unarchiveObjectWithData:pbData];
		
		[[self cell] setGradient:[dict objectForKey:@"Gradient"]];
		[[self cell] setGdType:[[dict objectForKey:@"Type"] intValue]];
		[[self cell] setAngle:[[dict objectForKey:@"Angle"] floatValue]];
 	}
	
	[self setNeedsDisplay:YES];
	
	return YES;
}

- (unsigned int)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	return NSDragOperationNone;
}

@end
