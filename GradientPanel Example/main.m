//
//  main.m
//  GradientPanel Example
//
//  Created by Raphael Bost on 17/07/12.
//  Copyright 2012 Ecole Polytechnique. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
