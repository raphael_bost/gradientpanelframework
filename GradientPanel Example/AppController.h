#import <Cocoa/Cocoa.h>
@class GPGradientWell;
@interface AppController : NSObject/* Specify a superclass (eg: NSObject or NSView) */ {
	IBOutlet GPGradientWell *well;
	IBOutlet GPGradientWell *well2;
	IBOutlet GPGradientWell *well3;
}
- (IBAction)show:(id)sender;
@end
