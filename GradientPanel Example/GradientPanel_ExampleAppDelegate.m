//
//  GradientPanel_ExampleAppDelegate.m
//  GradientPanel Example
//
//  Created by Raphael Bost on 17/07/12.
//  Copyright 2012 Ecole Polytechnique. All rights reserved.
//

#import "GradientPanel_ExampleAppDelegate.h"

@implementation GradientPanel_ExampleAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

@end
