#import "AppController.h"
#import "GPGradientPanel.h"
#import "GPGradientWell.h"
#import "CTGradient.h"

@implementation AppController

- (IBAction)show:(id)sender {
	[[GPGradientPanel sharedGradientPanel] showWindow:sender];
}


- (void)awakeFromNib
{
	[[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
	[well setGradient:[[NSGradient alloc] initWithStartingColor:[NSColor blueColor] endingColor:[NSColor whiteColor]] ];
//	[well2 setGradient:[NSGradient gradientWithBeginningColor:[NSColor redColor] endingColor:[NSColor orangeColor]]];
//	[well3 setGradient:[NSGradient gradientWithBeginningColor:[NSColor blueColor] endingColor:[NSColor purpleColor]]];
}
@end
