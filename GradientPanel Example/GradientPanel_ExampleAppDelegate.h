//
//  GradientPanel_ExampleAppDelegate.h
//  GradientPanel Example
//
//  Created by Raphael Bost on 17/07/12.
//  Copyright 2012 Ecole Polytechnique. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface GradientPanel_ExampleAppDelegate : NSObject <NSApplicationDelegate> {
@private
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
